const precoCombustivel = 5.00;
const consumoMedioPorLitro = 7;
let distanciaDaViagemKm = 200;

let litrosConsumidos = distanciaDaViagemKm / consumoMedioPorLitro;
let valorGastoDaViagem = litrosConsumidos * precoCombustivel;

console.log(valorGastoDaViagem.toFixed(2));