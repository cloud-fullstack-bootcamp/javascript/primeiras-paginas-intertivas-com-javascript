//numeros pares

let numero = 10;
let numeroPar = (numero % 2) === 0;

console.log(numeroPar);

if (numeroPar) {
    console.log("Numero par");
}

if (!numeroPar) {
    console.log("Numero impar");
}