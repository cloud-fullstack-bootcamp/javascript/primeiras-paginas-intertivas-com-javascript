/*
Calcular o IMC: IMC = peso / (altura * altura)

abaixo de 18.5 - abaixo do peso;
entre 18.5 e 25 Peso nomral;
entre 25 e 30 acima do peso;
entre 30 e 40 obeso
acima de 40 obesidade grave;
*/
const abaixoPeso = 'Abaixo do Peso';
const pesoNormal = 'Peso normal';
const acimaDoPeso = 'Acima do Peso';
const obeso = 'Obeso';
const obesidadeGrave = 'Obesidade Grave';

let altura = 1.75;
let peso = 340;
let imc = peso / (Math.pow(altura, 2))

if (imc < 18.5) {
    return console.log(abaixoPeso);
} else if (imc >= 18.5 && imc < 25) {
    console.log(pesoNormal);
} else if (imc >= 25 && imc < 30) {
    return console.log(acimaDoPeso);
} else if (imc >= 30 && imc < 40) {
    return console.log(obeso);
} else {
    return console.log(obesidadeGrave);
}
