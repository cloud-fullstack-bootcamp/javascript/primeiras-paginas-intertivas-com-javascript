

const precoEtanol = 5.79;
const precoGasolina = 6.20;

const kmPorLitro = 12;
const distanciaEmKm = 100;
const etanol = 'etanol';
const gasolina = 'gasolina';
const tipoConbustivel = gasolina;

const listrosConsumidos = distanciaEmKm / kmPorLitro;

if (tipoConbustivel === "Etanol") {
    const valorGasto = listrosConsumidos * precoEtanol;
    console.log(valorGasto.toFixed(2));
} else {
    const valorGasto = listrosConsumidos * precoGasolina;
    console.log(valorGasto.toFixed(2));
}


