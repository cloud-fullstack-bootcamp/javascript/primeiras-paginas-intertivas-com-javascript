/*
1) Faça um algoritimo que dado as 3 notas tiradas por um alino em um semeestre da faculdade calicle e imprime a sua media ea sua classificação confomr a tabela abaixo:

media - (nota1 + nota2+ nota3) /3;

classificação:
- media menor que 5, reprovado;
media entre 5 e menor que 7, recuperação;
media igual ou acima de 7, passou de semestre;

*/

let nota1 = 5;
let nota2 = 8;
let nota3 = 9;
let media = (nota1 + nota2 + nota3) / 3;

if (media < 5) {
    console.log('Reprovado');
}else if (media >= 5 && media < 7) {
    console.log('recuperação');
} else {
    console.log('Aprovado');
}