

function sayMyName(name) {
    console.log('your name is: ' + name);
}

function quadrado(valor) {
    return valor * valor;
}

function incrementarJuros(valor, quantosPorCento) {
    const valorDeAcrecimo = (quantosPorCento / 100) * valor;
    return valor + valorDeAcrecimo;
}

sayMyName('Edivan');
console.log(quadrado(5));
console.log(incrementarJuros(200, 10));