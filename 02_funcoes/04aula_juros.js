/*
    Elabore um algoritmo que calcle oque dever ser pago por um produto, considerando o preço normal de etiqueta e a escolhar da condição de pagamento.
    Utilize os codigos da tabela a seguir para ler qual a condição de pagamento esolhida e efetuar a cáluclo adequado.

    coidog:
    1 - a vista debito, rece 10% de desconto;
    2 - a vista no dinheiro ou PIX, receber 15% de desconto;
    3 - em duas veze, preço normal de etiqueta sem juros;
    4 - acima de duas vezes, precço normal de etiqueta mais juros de 10%.

    */

let camisa = 100;
let bola = 120;

const avistaDebito = 1;
const avisaDinheiroOuPix = 2;
const parcelaDuasVezes = 3;
const parcelarAcimaDeDuas = 4;

function aplicarDesconto(valorProduto, desconto) {
    return valorProduto - (valorProduto * (desconto / 100));
}

function aplicarJuros(valorProduto, valorJuros) {
    return valorProduto + (valorProduto * (valorJuros / 100));
}

function formaDePagamento(opcaoDepagamento){
    
    if (opcaoDepagamento == avistaDebito) {
        return console.log('Valor á se pago R$ ' + aplicarDesconto(100, 10).toFixed(2))
    } else if (opcaoDepagamento == avisaDinheiroOuPix) {
        return console.log('Valor á se pago R$ ' + aplicarDesconto(100, 15).toFixed(2));
    } else if (opcaoDepagamento == parcelaDuasVezes) {
        return console.log('Valor á se pago R$ ' + camisa.toFixed(2));
    } else {
        return console.log('Valor a se pago R$ ' + aplicarJuros(100, 10).toFixed(2));
    }
}

formaDePagamento(4);

