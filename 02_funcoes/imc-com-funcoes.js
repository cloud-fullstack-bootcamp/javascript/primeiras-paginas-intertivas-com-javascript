/*
Calcular o IMC: IMC = peso / (altura * altura)

abaixo de 18.5 - abaixo do peso;
entre 18.5 e 25 Peso nomral;
entre 25 e 30 acima do peso;
entre 30 e 40 obeso
acima de 40 obesidade grave;
*/


const abaixoPeso = 'Abaixo do Peso';
const pesoNormal = 'Peso normal';
const acimaDoPeso = 'Acima do Peso';
const obeso = 'Obeso';
const obesidadeGrave = 'Obesidade Grave';


function calcularIMC(peso, altura) {
    return peso / Math.pow(altura, 2);
}

function classificarIMC(imc) {
    if (imc < 18.5) {
        return abaixoPeso;
    } else if (imc >= 18.5 && imc < 25) {
        return pesoNormal;
    } else if (imc >= 25 && imc < 30) {
        return acimaDoPeso;
    } else if (imc >= 30 && imc < 40) {
        return obeso;
    } else {
        return obesidadeGrave;
    }
}

// função principal; nao precisa por [main] pois ele se alta chama automaticamente;
(function () {
    let altura = 1.75;
    let peso = 75;

    let imc = calcularIMC(peso, altura);
    console.log(classificarIMC(imc));
})()


//main();



