
const alunos = {
    nome: 'Edivan Carvalho',
    idade: 34,
    descrever: function(){
        this
        console.log(`Meu nome é ${this.nome} e minha idade ${this.idade}`);
    }
};

console.log(alunos.nome, alunos.idade);
console.log(alunos);

alunos.altura = 1.75;

console.log(alunos);

alunos.descrever();
console.log('-----------------------');
console.log(alunos['nome'], alunos['idade']);