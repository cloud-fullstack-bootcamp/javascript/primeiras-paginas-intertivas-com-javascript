class Pessoa{
    nome;
    idade;
    anoDenascimento;

    constructor (nome, idade){
        this.nome = nome;
        this.idade = idade;
        this.anoDenascimento = 2022 - idade;
    }
    
    descrever(){
        console.log(`Meu nome é ${this.nome} e tenho ${this.idade} anos.`);
    }
}

const edivan = new Pessoa('Edivan', 35);
console.log(edivan);
