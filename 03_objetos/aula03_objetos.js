class Pessoa{
    nome;
    idade;
    anoDenascimento;

    constructor (nome, idade){
        this.nome = nome;
        this.idade = idade;
        this.anoDenascimento = 2022 - idade;
    }
    
    descrever(){
        console.log(`Meu nome é ${this.nome} e tenho ${this.idade} anos.`);
    }
}

function compararPessoas(pessoa1, pessoa2){
    if (pessoa1 > pessoa2){
        console.log(`${pessoa1.nome} é mais velha que ${pessoa2.nome}`);
    }else if (pessoa2 > pessoa1) {
        console.log(`${pessoa2.nome} é mais velha que ${pessoa1.nome}`);
    }else{
        console.log(`${pessoa1.nome} e ${pessoa2.nome} tem a mesma idade.`);
    }
}

const alunos = new Pessoa('Ana', 23);
const alunos1 = new Pessoa('Joao', 23);

compararPessoas(alunos, alunos1);
