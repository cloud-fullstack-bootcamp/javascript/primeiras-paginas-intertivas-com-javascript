class Carro {
    marca;
    cor;
    gastoMedioPorKm;

    constructor(marca, cor, gastoMedioPorKm) {
        this.marca = marca;
        this.cor = cor;
        this.gastoMedioPorKm = 1 / gastoMedioPorKm;
    }

    calcularGastoDaViagem(distanciaKm, valorDoCombustivel) {
        return distanciaKm * this.gastoMedioPorKm * valorDoCombustivel;
    }
}

const uno = new Carro('uno', 'Prata', 12);
console.log(`Valor R$ ${uno.calcularGastoDaViagem(70, 5).toFixed(2)} da viagem em gasolina`);
const fuscar = new Carro('Fx', 'Branco', 15);
console.log(`Valor R$ ${fuscar.calcularGastoDaViagem(70, 5).toFixed(2)} da viagem em gasolina.`);
