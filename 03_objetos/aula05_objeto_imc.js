const abaixoPeso = 'Abaixo do Peso';
const pesoNormal = 'Peso normal';
const acimaDoPeso = 'Acima do Peso';
const obeso = 'Obeso';
const obesidadeGrave = 'Obesidade Grave';

class Pessoa {

    nome;
    peso;
    altura;

    constructor(nome, peso, altura) {
        this.nome = nome;
        this.peso = peso;
        this.altura = altura;
    }

    calcularIMC(peso, altura) {
        return peso / (Math.pow(altura, 2));
    }

    classificarImc() {
        let imc = this.calcularIMC();

        if (imc < 18.5) {
            return abaixoPeso;
        } else if (imc >= 18.5 && imc < 25) {
            return pesoNormal;
        } else if (imc >= 25 && imc < 30) {
            return acimaDoPeso;
        } else if (imc >= 30 && imc < 40) {
            return obeso;
        } else {
            return obesidadeGrave;
        }
    }
}

const pessoa = new Pessoa('José', 70, 1.75);
console.log(pessoa.calcularIMC(pessoa.peso, pessoa.altura).toFixed(2));

const pessoa01 = new Pessoa('Edivan', 88, 1.75);
console.log(pessoa.calcularIMC(pessoa01.peso, pessoa01.altura).toFixed(2));

const pessoa02 = new Pessoa('Edivan', 88, 1.75);
console.log(pessoa.classificarImc(pessoa02.peso, pessoa02.altura));