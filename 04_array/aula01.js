
const aluno = ['joao', 'anna', 'edivan'];

console.log(aluno);
console.log(aluno.length);

aluno.push('carvalho');
console.log(aluno);

aluno.pop(); // remove o ultimo;
console.log(aluno);

aluno.shift(); // remove o primeiro
console.log(aluno);

//--------------------
console.log('==================================');
const notas = [];

notas.push(5);
notas.push(7);
notas.push(8);
notas.push(2);
notas.push(5);

console.log(notas);
console.log(notas.length);

