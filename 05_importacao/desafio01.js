const { gets, print } = require('./funcoes-auxiliares');

/*
    uma sala contem 5 alunos e para cada aluno foi sorteado um numero de 1 -100.
    faça um progrma que recebar os numeros sorteados para os alunos e mostre o amio numero sorteado.

    dadps de entrada:
    5
    50
    10
    98
    23

    saida: 
    98
*/

const quantidadeDeAlunos = gets();

const numerosSorteados = [];
for (let i = 0; i < quantidadeDeAlunos; i++) {
    const numeroSorteado = gets();
    numerosSorteados.push(numeroSorteado);
}
print(numerosSorteados);

let maiorValor = 0;

for (let i = 0; i < numerosSorteados.length; i++) {
    const numeroSorteado = numerosSorteados[i];
    if (numeroSorteado > maiorValor) {
        maiorValor = numeroSorteado;
    }
}

print(maiorValor);
