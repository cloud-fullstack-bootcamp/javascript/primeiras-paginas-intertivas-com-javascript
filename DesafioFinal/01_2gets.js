// IMPORTANTE: As funções "gets" e "print" são acessíveis globalmente, onde: 
// - "gets" : lê UMA linha com dado(s) de entrada (inputs) do usuário;
// - "print": imprime um texto de saída (output) e pula uma linha ("\n") automaticamente.

let N = parseInt(gets([3,4,6,9,10]))
let pares = [];
let impares = [];


for (let i = 0; i < N.length; i++) {
    let num = parseInt(gets())
    
    num % 2 === 0 ? pares.push(num) : impares.push(num)
}  


pares.sort((a,b) => a -b )
impares.sort((a,b) => b - a) 

for(e of [...pares,...impares]){
  console.log(e)
}