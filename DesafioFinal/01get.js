// IMPORTANTE: As funções "gets" e "print" são acessíveis globalmente, onde: 
// - "gets" : lê UMA linha com dado(s) de entrada (inputs) do usuário;
// - "print": imprime um texto de saída (output) e pula uma linha ("\n") automaticamente.

let N = parseInt(gets())

let vetor = [3,4,6,9,10];
let pares = [];
let impares = [];

//TODO: Implemente uma condição para o armazenamento dos números PARES e ÍMPARES:

for (let i = 0; i < vetor.length; i++) {
    const aux = vetor[i];
    if(aux % 2 === 0){
        
        pares.push(aux)
    }else{
        impares.push(aux)
    }  
    
}
console.log('Vetor: ' + vetor)
console.log('Par(es): ' + pares)
console.log('Impar(es): ' + impares)


//TODO: Imprima os valores dos três vetores um em cada linha: