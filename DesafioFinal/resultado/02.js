// fonte: https://web.dio.me/coding/desafio-javascript-warburg-pincus-2/algorithm/inteiro-ou-decimal?back=/track/cloud-fullstack-bootcamp-warburg-pincus&tab=undefined&moduleId=undefined

// IMPORTANTE: As funções "gets" e "print" são acessíveis globalmente, onde: 
// - "gets" : lê UMA linha com dado(s) de entrada (inputs) do usuário;
// - "print": imprime um texto de saída (output) e pula uma linha ("\n") automaticamente.


let valor = gets().replace(",",".");

print(parseInt(valor) != parseFloat(valor) ? "Decimal" : "Inteiro")

//TODO: Descubra se o valor é inteiro ou decimal