
// As entradas serão datas em formato numeral. O dia ou mês que possuirem somente um digito, terão um 0 (zero) na frente.

// Saida
// As saídas serão as mesmas datas da entrada porém, com o mês escrito por extenso. O mês deve ter a primeira letra em maiúsculo.

const meses = {
    "1": "Janeiro",
    "2": "Feveiro",
    "3": "Março",
    "4": "Abril",
    "5": "Maio",
    "6": "Junho",
    "7": "Julho",
    "8": "Agosto",
    "9": "Setembro",
    "10": "Outubro",
    "11": "Novembro",
    "12": "Dezembro"
  
  }
  
  let data = gets();
  let [dia, mes ,ano] = data.split('/');
  
  
  console.log(`${dia} de ${meses[parseInt(mes)]} de ${ano}`)