// primeira linha deverá receber o valor marcado do produto, e na segunda linha o valor do produto com o desconto já aplicado.

// Saída
// A saída deverá retornar o percentual de desconto que foi aplicado no produto, conforme exemplo abaixo.

let M, S; 

M = parseInt(gets()); 

S = parseInt(gets()); 

let percentual = (S * 100 / M)
console.log(`O desconto foi de ${100 - percentual}%`)
